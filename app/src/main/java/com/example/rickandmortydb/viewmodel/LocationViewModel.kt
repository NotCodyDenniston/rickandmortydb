package com.example.rickandmortydb.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rickandmortydb.model.RickRepo
import com.example.rickandmortydb.model.remote.dtos.episodedtos.EpisodeDTO
import com.example.rickandmortydb.model.remote.dtos.locationdtos.LocationDTO
import com.example.rickandmortydb.util.Resource
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class LocationViewModel( private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val repo = RickRepo

    private val _locationState: MutableStateFlow<Resource<List<LocationDTO>>> = MutableStateFlow(
        Resource.Loading())
    val locationState = _locationState.asStateFlow()
    init {
        getData()
    }

    private fun getData() = viewModelScope.launch {
        _locationState.value = repo.getLocation()
    }
}