package com.example.rickandmortydb.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rickandmortydb.model.RickRepo
import com.example.rickandmortydb.model.remote.dtos.rickdtos.RickDTO
import com.example.rickandmortydb.util.Resource
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class RickViewModel: ViewModel() {

    private val repo = RickRepo

    private val _characters: MutableStateFlow<Resource<List<RickDTO>>> =
        MutableStateFlow(Resource.Loading())
    val characterState: StateFlow<Resource<List<RickDTO>>>
        get() = _characters

    init {
        getCharacters()
    }

    private fun getCharacters() = viewModelScope.launch {
        _characters.value = repo.getCharacters()
    }
}