package com.example.rickandmortydb.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.rickandmortydb.model.RickRepo
import com.example.rickandmortydb.model.remote.dtos.episodedtos.EpisodeDTO
import com.example.rickandmortydb.util.Resource
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class EpisodeViewModel(
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    private val repo = RickRepo

    private val _episodeState: MutableStateFlow<Resource<List<EpisodeDTO>>> = MutableStateFlow(Resource.Loading())
    val episodeState = _episodeState.asStateFlow()
    init {
        getData()
    }

    private fun getData() = viewModelScope.launch {
        _episodeState.value = repo.getEpisodes()
    }
}