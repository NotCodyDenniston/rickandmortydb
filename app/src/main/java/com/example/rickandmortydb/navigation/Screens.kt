package com.example.rickandmortydb.navigation

sealed class Screens(val route: String){
    object RickListScreen : Screens("ricklist_route")
    object RickDetailScreen: Screens("rickdetail_route/{rickID}"){
        fun passRick(rickID: Int): String {
            return "rickdetail_route/${rickID}"
        }
    }
    object RickLocationScreen: Screens("ricklocation_route/{locationID}"){
    fun passLocation(locationID: String): String {
        return "ricklocation_route/${locationID}"
    }
    }
    object RickEpisodeScreen: Screens("rickepisode_route/{episodeID}"){
        fun passEpisode(episodeID: String): String {
            return "rickepisode_route/${episodeID}"
        }
    }
}
