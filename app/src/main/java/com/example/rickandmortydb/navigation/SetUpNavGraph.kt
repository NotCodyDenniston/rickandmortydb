package com.example.rickandmortydb.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.example.rickandmortydb.view.rickdetails.RickDetails
import com.example.rickandmortydb.view.rickepisode.RickEpisode
import com.example.rickandmortydb.view.ricklist.RickList
import com.example.rickandmortydb.view.ricklocation.RickLocation

@Composable
fun SetUpNavGraph(
    navController: NavHostController
) {
    NavHost(
        navController = navController,
        startDestination = Screens.RickListScreen.route
    ) {
        composable(Screens.RickListScreen.route) {
            RickList(navigate = {
                navController.navigate(Screens.RickDetailScreen.passRick(it.id))
            })
        }
        composable(
            Screens.RickDetailScreen.route,
            arguments = listOf(navArgument("rickID") {
                type = NavType.IntType
            })
        ) {  entry ->
            RickDetails(navigateLocation = {
                navController.navigate(Screens.RickLocationScreen.passLocation(it))
            },
            navigateEpisode = {

                navController.navigate(Screens.RickEpisodeScreen.passEpisode(it))
            },
            rickID = entry.arguments?.getInt("rickID"))
        }
        composable(
            Screens.RickEpisodeScreen.route,
            arguments = listOf(navArgument("episodeID") {
                type = NavType.StringType
            })

        ){  entry ->
            RickEpisode(episodeID = entry.arguments?.getString("episodeID"))
        }
        composable(
            Screens.RickLocationScreen.route,
            arguments = listOf(navArgument("locationID") {
                type = NavType.StringType
            })
        ){  entry ->
            RickLocation(locationID = entry.arguments?.getString("locationID") )
        }
    }
}