package com.example.rickandmortydb.model.remote.dtos.locationdtos

@kotlinx.serialization.Serializable
data class InfoDTO(
    val count: Int,
    val next: String,
    val pages: Int,
    val prev: String?
)