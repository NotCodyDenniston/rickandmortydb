package com.example.rickandmortydb.model.remote.dtos.episodedtos

@kotlinx.serialization.Serializable
data class Info(
    val count: Int,
    val next: String,
    val pages: Int,
    val prev: String?
)