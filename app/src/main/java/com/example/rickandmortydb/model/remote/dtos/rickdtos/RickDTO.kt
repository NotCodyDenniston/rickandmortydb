package com.example.rickandmortydb.model.remote.dtos.rickdtos

import com.example.rickandmortydb.model.remote.dtos.locationdtos.LocationDTO

@kotlinx.serialization.Serializable
data class RickDTO(
    val created: String,
    val episode: List<String>,
    val gender: String,
    val id: Int,
    val image: String,
    val location: LocationDTO,
    val name: String,
    val origin: OriginDTO,
    val species: String,
    val status: String,
    val type: String,
    val url: String
)