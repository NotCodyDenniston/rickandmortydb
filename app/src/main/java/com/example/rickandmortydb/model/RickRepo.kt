package com.example.rickandmortydb.model

import com.example.rickandmortydb.model.remote.RetrofitObject
import com.example.rickandmortydb.model.remote.RickService
import com.example.rickandmortydb.model.remote.dtos.episodedtos.EpisodeDTO
import com.example.rickandmortydb.model.remote.dtos.episodedtos.EpisodeResponse
import com.example.rickandmortydb.model.remote.dtos.locationdtos.LocationDTO
import com.example.rickandmortydb.model.remote.dtos.locationdtos.LocationResponse
import com.example.rickandmortydb.model.remote.dtos.rickdtos.RickDTO
import com.example.rickandmortydb.model.remote.dtos.rickdtos.SomeRicksResponse
import com.example.rickandmortydb.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

object RickRepo {
    private val service: RickService by lazy { RetrofitObject.getRickService() }

    suspend fun getCharacters(): Resource<List<RickDTO>> = withContext(Dispatchers.IO) {
        val fetchResponse: Response<SomeRicksResponse> =
            service.getSomeCharacters().execute()

        return@withContext if (fetchResponse.isSuccessful && fetchResponse.body() != null) {
            val rickResponse = fetchResponse.body()!!
            val rickList: List<RickDTO> = rickResponse.results

            Resource.Success(rickList)
        } else {
            Resource.Error(fetchResponse.message())
        }
    }
    suspend fun getEpisodes(): Resource<List<EpisodeDTO>> = withContext(Dispatchers.IO) {
        val fetchResponse: Response<EpisodeResponse> =
            service.getAllEpisodes().execute()

        return@withContext if (fetchResponse.isSuccessful) {
            val episodeResponse = fetchResponse.body() ?: EpisodeResponse()
            val episodeList: List<EpisodeDTO> = episodeResponse.results

            Resource.Success(episodeList)
        } else {
            Resource.Error(fetchResponse.message())
        }
    }
    suspend fun getLocation(): Resource<List<LocationDTO>> = withContext(Dispatchers.IO) {
        val fetchResponse: Response<LocationResponse> =
            service.getAllLocations().execute()

        return@withContext if (fetchResponse.isSuccessful) {
            val locationResponse = fetchResponse.body() ?: LocationResponse()
            val locationList: List<LocationDTO> = locationResponse.results

            Resource.Success(locationList)
        } else {
            Resource.Error(fetchResponse.message())
        }
    }

}