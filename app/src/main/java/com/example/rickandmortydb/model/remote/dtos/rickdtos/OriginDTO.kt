package com.example.rickandmortydb.model.remote.dtos.rickdtos

@kotlinx.serialization.Serializable
data class OriginDTO(
    val name: String,
    val url: String
)