package com.example.rickandmortydb.model.remote

sealed class NetworkResponse<T> {
    sealed class Success<T>(
        val successList: List<T>
    ) : NetworkResponse<List<T>>() {

        data class RickSuccess<RickDTO>(val rickList: List<RickDTO>) :
            Success<RickDTO>(rickList)

    }

    data class Error(val message: String) : NetworkResponse<String>()
}