package com.example.rickandmortydb.model.remote.dtos.rickdtos

//@kotlinx.serialization.Serializable
//class SomeRicksResponse: ArrayList<RickDTO>()

@kotlinx.serialization.Serializable
data class SomeRicksResponse(
    val results: List<RickDTO>
)