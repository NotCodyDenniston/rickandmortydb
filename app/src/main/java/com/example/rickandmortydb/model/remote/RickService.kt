package com.example.rickandmortydb.model.remote

import com.example.rickandmortydb.model.remote.dtos.episodedtos.EpisodeResponse
import com.example.rickandmortydb.model.remote.dtos.locationdtos.LocationResponse
import com.example.rickandmortydb.model.remote.dtos.rickdtos.SomeRicksResponse
import retrofit2.Call
import retrofit2.http.GET

interface RickService {

    @GET(CHARACTER_ENDPOINT)
    fun getSomeCharacters(): Call<SomeRicksResponse>

    @GET(EPISODE_ENDPOINT)
    fun getAllEpisodes(): Call<EpisodeResponse>

    @GET(LOCATION_ENDPOINT)
    fun getAllLocations(): Call<LocationResponse>


    companion object{

        private const val CHARACTER_ENDPOINT = "character/?count=15"
        private const val EPISODE_ENDPOINT = "episode/"
        private const val LOCATION_ENDPOINT = "location/"
    }
}