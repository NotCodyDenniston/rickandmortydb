package com.example.rickandmortydb.model.remote.dtos.episodedtos

@kotlinx.serialization.Serializable
data class EpisodeResponse(
    val info: Info? = null,
    val results: List<EpisodeDTO> = emptyList()
)