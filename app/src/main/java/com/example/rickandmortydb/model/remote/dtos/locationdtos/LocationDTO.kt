package com.example.rickandmortydb.model.remote.dtos.locationdtos

@kotlinx.serialization.Serializable
data class LocationDTO(
    val created: String,
    val dimension: String,
    val id: Int,
    val name: String,
    val residents: List<String>,
    val type: String,
    val url: String
)