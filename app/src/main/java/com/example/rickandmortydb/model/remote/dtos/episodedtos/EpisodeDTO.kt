package com.example.rickandmortydb.model.remote.dtos.episodedtos

@kotlinx.serialization.Serializable
data class EpisodeDTO(
    val air_date: String,
    val characters: List<String>,
    val created: String,
    val episode: String,
    val id: Int,
    val name: String,
    val url: String
)