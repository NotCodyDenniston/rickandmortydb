package com.example.rickandmortydb.model.remote.dtos.locationdtos

@kotlinx.serialization.Serializable
data class LocationResponse(
    val info: InfoDTO? = null,
    val results: List<LocationDTO> = emptyList()
)