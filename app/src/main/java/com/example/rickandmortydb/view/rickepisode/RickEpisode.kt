package com.example.rickandmortydb.view.rickepisode

import android.util.Log
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.rickandmortydb.model.remote.dtos.episodedtos.EpisodeDTO
import com.example.rickandmortydb.util.Resource
import com.example.rickandmortydb.view.ricklist.RickList
import com.example.rickandmortydb.viewmodel.EpisodeViewModel

@Composable
fun RickEpisode(
    episodeID: String?,
    EpisodeViewModel: EpisodeViewModel = viewModel()
){


    when(val episodeState = EpisodeViewModel.episodeState.collectAsState().value){
        is Resource.Error -> {

        }
        is Resource.Loading -> {

        }
        is Resource.Success -> {
            val rickEpisodes = episodeState.data.filter { episode -> episodeID?.toInt() == episode.id }
            ShowItems(episode = rickEpisodes[0])
        }
    }
}

@Composable
fun ShowItems(episode: EpisodeDTO){
    Column(

    ) {
        Text(text = "Name: ${episode.name}")
        Text(text = "Episode: ${episode.episode}")
        Text(text = "Created on: ${episode.created}")
        Text(text = "Air Date: ${episode.air_date}")
        Text(text = "Characters: ${episode.characters}")

    }
}