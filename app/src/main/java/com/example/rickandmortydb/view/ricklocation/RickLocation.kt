package com.example.rickandmortydb.view.ricklocation

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.AsyncImage
import com.example.rickandmortydb.model.remote.dtos.locationdtos.LocationDTO
import com.example.rickandmortydb.model.remote.dtos.rickdtos.RickDTO
import com.example.rickandmortydb.util.Resource
import com.example.rickandmortydb.view.rickdetails.ShowItems
import com.example.rickandmortydb.viewmodel.LocationViewModel
import com.example.rickandmortydb.viewmodel.RickViewModel

@Composable
fun RickLocation(
    locationID: String?,
    LocationViewModel: LocationViewModel = viewModel()
){


    when(val locationState = LocationViewModel.locationState.collectAsState().value){
        is Resource.Error -> {

        }
        is Resource.Loading -> {

        }
        is Resource.Success -> {
            val location = locationState.data.filter { location -> location.id == locationID?.toInt() }
            ShowItems(location = location[0])
        }
    }
}

@Composable
fun ShowItems(location: LocationDTO){

    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally){
        Text(text = "${location.name}")
        Text(text = "Located in dimension ${location.dimension}")
        Text(text = "Created: ${location.created}")
        Text(text = "Type: ${location.type}")

    }

}

