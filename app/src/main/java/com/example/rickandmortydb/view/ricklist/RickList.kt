package com.example.rickandmortydb.view.ricklist

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.AsyncImage
import com.example.rickandmortydb.model.remote.dtos.rickdtos.RickDTO
import com.example.rickandmortydb.util.Resource
import com.example.rickandmortydb.viewmodel.RickViewModel

@Composable
fun RickList(
    navigate: (rick: RickDTO) -> Unit,
    RickViewModel: RickViewModel = viewModel()
){

    when(val characterState = RickViewModel.characterState.collectAsState().value){
        is Resource.Error -> {

        }
        is Resource.Loading -> {

        }
        is Resource.Success -> {
            ShowItems(data = characterState.data, navigate = navigate)
        }
    }
}

@Composable
fun ShowItems(
    data: List<RickDTO>,
    navigate: (rickDTO: RickDTO) -> Unit
){

    LazyColumn(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally){
        items(data){rick: RickDTO ->
            Text(text = "${rick.name}")
            AsyncImage(model = "${rick.image}",
                contentDescription = null,
                modifier = Modifier.clickable {navigate(rick)})
        }
    }
}