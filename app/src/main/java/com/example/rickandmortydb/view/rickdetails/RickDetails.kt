package com.example.rickandmortydb.view.rickdetails

import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.core.text.isDigitsOnly
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.AsyncImage
import com.example.rickandmortydb.model.remote.dtos.episodedtos.EpisodeDTO
import com.example.rickandmortydb.model.remote.dtos.rickdtos.RickDTO
import com.example.rickandmortydb.util.Resource
import com.example.rickandmortydb.viewmodel.RickViewModel

@Composable
fun RickDetails(
    rickID: Int?,
    navigateLocation: (location: String) -> Unit,
    navigateEpisode: (episode: String) -> Unit,
    RickViewModel: RickViewModel = viewModel()
){
    

    when(val characterState = RickViewModel.characterState.collectAsState().value){
        is Resource.Error -> {

        }
        is Resource.Loading -> {

        }
        is Resource.Success -> {
            val rick = characterState.data.filter { char -> char.id == rickID }
            val episodeID = rick[0].episode[0].filter {
                it.isDigit()
            }
            ShowItems(rick = rick[0],
                navigateLocation = navigateLocation,
                navigateEpisode = navigateEpisode,
                episodeiD = episodeID)
        }
    }
}

@Composable
fun ShowItems(
    rick: RickDTO,
    navigateLocation: (location: String) -> Unit,
    navigateEpisode: (episode: String) -> Unit,
    episodeiD: String

){

        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally){
                Text(text = "${rick.name}")
                AsyncImage(model = "${rick.image}",
                    contentDescription = null,
                    modifier = Modifier
                        .size(200.dp)
                        .padding(bottom = 10.dp)
                   )
                Text(text = "${rick.gender}")
                Text(text = "${rick.species}")
                Text(text = "${rick.status}", Modifier.padding(bottom = 50.dp))
                Text(text = "Location: ${rick.location.name}")
            val locationID = rick.location.url.filter { it.isDigit()}
                Button(onClick = { navigateLocation(locationID) }) {
                    Text(text = "Location")
                }
                Text(text = "Episode:")
                Button(onClick = { navigateEpisode(episodeiD) }) {
                    Text(text = "Episode")
                }
            }
}



//val created: String,
//val episode: List<String>,
//val gender: String,
//val id: Int,
//val image: String,
//val location: LocationDTO,
//val name: String,
//val origin: OriginDTO,
//val species: String,
//val status: String,
//val type: String,
//val url: String